from rest_framework import serializers

from user_accounts.models import CustomUser
from django.contrib.auth.models import Group

# Helper function
def createUser(username, name, email, password):
    user = CustomUser.objects.create(
                username=username,
                email=email,
                name=name,
            )

    user.set_password(password)
    user.save()
    return user
# End

class UserSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(
        allow_blank=False,
        label="Email Address",
        max_length=100,
        required=True,
    )

    class Meta:
        model = CustomUser
        fields = ('id','username', 'password', 'email', 'name')

        extra_kwargs = {
            'password': {
                'write_only': True
            },
        }

    def create(self, validated_data):
        user = createUser(validated_data['username'],
                            validated_data['name'],
                            validated_data['email'],
                            validated_data['password']
                            )

        volunteer_group = Group.objects.get(name='Volunteer')
        user.groups.add(volunteer_group)
        user.save()

        return user


class UserSerializerBasic(UserSerializer):

    class Meta:
        model = CustomUser
        fields = ('email', 'name')
