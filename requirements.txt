Django==2.2.6
Pillow==6.2.1
djangorestframework==3.10.3
django-environ==0.4.5
djangorestframework-simplejwt==4.3.0
gunicorn==19.9.0
