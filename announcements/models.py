from django.db import models
from user_accounts.models import CustomUser


class Announcement(models.Model):
    user = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='users'
    )
    title = models.CharField(max_length=100,help_text='News title')
    contents = models.CharField(
        max_length=500,
        help_text='News message'
    )
    hash_tags = models.CharField(max_length=100,help_text='hash tags')
    is_active = is_active = models.BooleanField(default=True)
    def __str__(self):
        return self.title

    
